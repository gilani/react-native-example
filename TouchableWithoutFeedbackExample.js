import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, Text, View } from 'react-native';
import { ItemList } from "./utils/constants"

const TouchableWithoutFeedbackExample = () => {

  const onPress = () => {
    console.log("You pressed it!")
  };

  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={onPress}>
        <ScrollView horizontal={true}>
          {
            ItemList.map(elem => {
              return (
                <Text>{elem}</Text>
              )
            })
          }
        </ScrollView>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  }
});

export default TouchableWithoutFeedbackExample;